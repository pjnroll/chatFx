/*
.setBackground(Background.EMPTY);
*/

package chatfx;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;import java.io.Serializable;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;

public class FXMLChatFxController {
    

    
/*
   RISERVATO
*/
private boolean isServer = false;  
private NetworkConnection nw;
       
//--------

private Server creaServer() {

   return new Server(Integer.valueOf(portaField.getText()), "Server", data -> {

       	Platform.runLater(() ->  {
       	chatArea.appendText(data.toString() + "\n");
           });
   	
   });
}

private Client creaClient() {
   
   return new Client(Integer.valueOf(portaField.getText()), hostField.getText(), "Server", data -> {
       Platform.runLater(() -> { chatArea.appendText(data.toString()+ "\n"); });
   });
}
/*
   RISERVATO
*/
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="principalePane"
    private AnchorPane principalePane; // Value injected by FXMLLoader

    @FXML // fx:id="menuPane"
    private AnchorPane menuPane; // Value injected by FXMLLoader

    @FXML // fx:id="imageMenu"
    private ImageView imageMenu; // Value injected by FXMLLoader

    @FXML // fx:id="loginPane"
    private AnchorPane loginPane; // Value injected by FXMLLoader

    @FXML // fx:id="hostField"
    private JFXTextField hostField; // Value injected by FXMLLoader

    @FXML // fx:id="portaField"
    private JFXTextField portaField; // Value injected by FXMLLoader

    @FXML // fx:id="btnToggle"
    private JFXToggleButton btnToggle; // Value injected by FXMLLoader

    @FXML // fx:id="usernameField"
    private JFXTextField usernameField; // Value injected by FXMLLoader

    @FXML // fx:id="passwordField"
    private JFXPasswordField passwordField; // Value injected by FXMLLoader

    @FXML // fx:id="btnConnetti"
    private JFXButton btnConnetti; // Value injected by FXMLLoader
    
    @FXML // fx:id="chatPane"
    private AnchorPane chatPane; // Value injected by FXMLLoader
    
    @FXML // fx:id="labelToggleBtn"
    private Label labelToggleBtn; // Value injected by FXMLLoader
    
    @FXML // fx:id="chatArea"
    private JFXTextArea chatArea; // Value injected by FXMLLoader

    @FXML // fx:id="chatField"
    private JFXTextField chatField; // Value injected by FXMLLoader

    @FXML // fx:id="btnInvia"
    private JFXButton btnInvia; // Value injected by FXMLLoader

    @FXML // fx:id="utentiPane"
    private AnchorPane utentiPane; // Value injected by FXMLLoader

    @FXML // fx:id="listUtenti"
    private JFXListView<?> listUtenti; // Value injected by FXMLLoader
    
    @FXML // fx:id="exitBtn"
    private ImageView exitBtn; // Value injected by FXMLLoader

    @FXML // fx:id="openChatFrame"
    private ImageView openChatFrame; // Value injected by FXMLLoader

    @FXML // fx:id="openLoginFrame"
    private ImageView openLoginFrame; // Value injected by FXMLLoader
       
    @FXML // fx:id="btnChiudiConnessione"
    private JFXButton btnChiudiConnessione; // Value injected by FXMLLoader

    @FXML // fx:id="openUtentiFrame"
    private ImageView openUtentiFrame; // Value injected by FXMLLoader
    
    @FXML // fx:id="closeMenuPane"
    private AnchorPane closeMenuPane; // Value injected by FXMLLoader

    @FXML // fx:id="yesCloseApp"
    private ImageView yesCloseApp; // Value injected by FXMLLoader

    @FXML // fx:id="noCloseApp"
    private ImageView noCloseApp; // Value injected by FXMLLoader
    
    //--------------------------METODI------------------------------------------
    
    
    @FXML
    void doExitApplication(MouseEvent event) {
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            if (!closeMenuPane.isVisible()) {
                closeMenuPane.setVisible(true);
            } else {
                closeMenuPane.setVisible(false);
            }
        }
    }
    
    @FXML
    void doNoCloseApp(MouseEvent event) {
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            closeMenuPane.setVisible(false);
        }
    }
    
    @FXML
    void doYesCloseApp(MouseEvent event) {
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            closeMenuPane.setVisible(true);
            System.exit(0);
        }
    }
    
    @FXML
    void doToggle(ActionEvent event) {

        if (btnToggle.isSelected()) {
            isServer = true;
            labelToggleBtn.setText("Host");
            hostField.setDisable(true);
            hostField.setText("127.0.0.1");
            usernameField.setText("root");
            passwordField.setText("root");
        } 
        if (!btnToggle.isSelected()) {
            isServer = false;
            labelToggleBtn.setText("Accedi");
            hostField.setDisable(false);
        }
   
    }
    
    @FXML
    void doConnetti(ActionEvent event) throws Exception {
                
        int portaConnessione = 6789;      
        
        
        if (btnToggle.isSelected() && isServer) {
            nw = creaServer();
            System.out.println("Sono un server");
        } 
        if (!btnToggle.isSelected() && (!isServer)){
            nw = creaClient();
            System.out.println("Sono un client");
        }
        
        nw.startConnection();
            
    }
        
    @FXML
    void doClickOnMenu(MouseEvent event) {
        
    }
    
    @FXML
    void doChiudiConnessione(ActionEvent event) throws Exception {
        nw.stopConnection();
    }   
    
//    @FXML
//    void doInvia(ActionEvent event) {
//        try {
//            String  messaggio = "";
//            
//            if (isServer) {
//                messaggio = "Server: "; 
//            } else {
//                messaggio = "Client: ";
//            }
//            
//            messaggio += chatField.getText();
//            chatField.clear();
//            System.out.println(messaggio);
//          //  chatArea.appendText(messaggio + "\n");
//            try {
//            	nw.send(messaggio);
//            } catch (Exception e) {
//            	e.printStackTrace();
//            }
//            
//            messaggio = "";
//            
//        } catch (Exception ex) {
//        	ex.printStackTrace();
//            chatArea.appendText("Messaggio non inviato\n");
//        }
//        
//    }
    @FXML
    void doInvia(ActionEvent e) {
    	btnInvia.setOnAction(event -> {
		String  messaggio = isServer ? "Server: " : "Client: ";
		messaggio += chatField.getText();
		chatField.clear();
		
		System.out.println(messaggio);
          try {
              nw.send((Serializable) messaggio);      
		  } catch (Exception ex) {
	        	ex.printStackTrace();
	            chatArea.appendText("Messaggio non inviato\n");
		  }
    	});
    }
    
    
    @FXML
    void doOpenChatFrame(MouseEvent event) {
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            if (!chatPane.isVisible()) {
                openChatFrame.setImage(new Image("chatfx/image/down-chevron.png"));
                chatPane.setVisible(true);
            } else {
                openChatFrame.setImage(new Image("chatfx/image/up-chevron.png")); 
                chatPane.setVisible(false);
            }
        }
    }

    @FXML
    void doOpenLoginFrame(MouseEvent event) {
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            if (!loginPane.isVisible()) {
                openLoginFrame.setImage(new Image("chatfx/image/right-chevron.png"));
                loginPane.setVisible(true);
            } else {
                openLoginFrame.setImage(new Image("chatfx/image/left-chevron.png")); 
                loginPane.setVisible(false);
            }
        }
    }

    @FXML
    void doOpenUtentiFrame(MouseEvent event) {

    }
    

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws Exception {
        assert principalePane != null : "fx:id=\"principalePane\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert menuPane != null : "fx:id=\"menuPane\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert imageMenu != null : "fx:id=\"imageMenu\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert exitBtn != null : "fx:id=\"exitBtn\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert openChatFrame != null : "fx:id=\"openChatFrame\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert openLoginFrame != null : "fx:id=\"openLoginFrame\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert loginPane != null : "fx:id=\"loginPane\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert hostField != null : "fx:id=\"hostField\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert portaField != null : "fx:id=\"portaField\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert btnToggle != null : "fx:id=\"btnToggle\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert usernameField != null : "fx:id=\"usernameField\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert passwordField != null : "fx:id=\"passwordField\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert labelToggleBtn != null : "fx:id=\"labelToggleBtn\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert btnConnetti != null : "fx:id=\"btnConnetti\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert chatPane != null : "fx:id=\"chatPane\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert chatArea != null : "fx:id=\"chatArea\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert chatField != null : "fx:id=\"chatField\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert btnInvia != null : "fx:id=\"btnInvia\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert btnChiudiConnessione != null : "fx:id=\"btnChiudiConnessione\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert openUtentiFrame != null : "fx:id=\"openUtentiFrame\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert utentiPane != null : "fx:id=\"utentiPane\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert listUtenti != null : "fx:id=\"listUtenti\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert closeMenuPane != null : "fx:id=\"closeMenuPane\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert yesCloseApp != null : "fx:id=\"yesCloseApp\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
        assert noCloseApp != null : "fx:id=\"noCloseApp1\" was not injected: check your FXML file 'FXMLChatFx.fxml'.";
      
        principalePane.setBackground(Background.EMPTY);
        closeMenuPane.setVisible(false);
        loginPane.setVisible(false);
        labelToggleBtn.setText("Login");
        chatPane.setVisible(false);
        utentiPane.setVisible(false);
        listUtenti.setBackground(Background.EMPTY);
       
      

    }
        
        
}

